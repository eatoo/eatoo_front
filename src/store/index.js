import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { layout } from "./layout";

Vue.use(Vuex);

const config = {
    headers: {
        "Content-type": "application/json"
    }
};

export default new Vuex.Store({
    modules: {
      layout
    },
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user: {},
        dish: {},
        userDish: {},
        userOrder: {},
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, token, user) {
            state.status = 'success';
            state.token = token;
            state.user = user
        },
        auth_error(state) {
            state.status = 'error'
        },
        dish_create(state) {
            state.status = 'success';
        },
        dish_receive(state, dish) {
            state.status = 'success';
            state.dish = dish
        },
        contact(state) {
            state.status = 'success';
        },
        create_order(state) {
            state.status = 'success';
        },
        user_dish_delete(state) {
            state.status = 'success';
        },
        user_dish_update(state) {
            state.status = 'success';
        },
        user_dish_receive(state, userDish) {
            state.status = 'success';
            state.userDish = userDish
        },
        user_order_receive(state, userOrder) {
            state.status = 'success';
            state.userOrder = userOrder
        },
        auth_modify(state, prof) {
            state.status = 'success';
            state.prof = prof
        },
        get_me(state, token) {
            state.status = 'success';
            state.token = token;
        },
        set_user(state, user) {
            state.user = user;
        },
        logout(state) {
            state.status = '';
            state.token = ''
        },
    },
    actions: {
        login({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.post("https://dev.api.eatoo.tech/auth/login", user, config)
                    .then(resp => {
                        const token = resp.data.token;
                        const user = resp.data.user;
                        localStorage.setItem('token', token);
                        axios.defaults.headers.common['Authorization'] = "Bearer " + token;
                        commit('auth_success', token, user);
                        commit('set_user', user);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('token');
                        reject(err)
                    })
            })
        },
        register({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.post("https://dev.api.eatoo.tech/auth/register", user, config)
                    .then(resp => {
                        const token = resp.data.token;
                        const user = resp.data.user;
                        commit('set_user', user);
                        localStorage.setItem('token', token);
                        axios.defaults.headers.common['Authorization'] = "Bearer " + token;
                        commit('auth_success', token, user);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('token');
                        reject(err)
                    })
            })
        },
        dishUpload({commit}, dish) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.post("https://dev.api.eatoo.tech/dishes/create", dish, config)
                    .then(resp => {
                        commit('dish_create');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        allDish({commit}) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.get("https://dev.api.eatoo.tech/dishes/all")
                    .then(resp => {
                        const dish = resp.data.dish;
                        commit('dish_receive', dish);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        searchDish({commit}, words) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.post("https://dev.api.eatoo.tech/dishes/searchWords", words)
                    .then(resp => {
                        const dish = resp.data.dish;
                        commit('dish_receive', dish);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        contact({commit}, form) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.post("https://dev.api.eatoo.tech/contact", form, config)
                    .then(resp => {
                        commit('contact');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        userDish({commit}) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.get("https://dev.api.eatoo.tech/auth/me?dishes")
                    .then(resp => {
                        const userDish = resp.data.dish;
                        commit('user_dish_receive', userDish);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        userOrder({commit}) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.get("https://dev.api.eatoo.tech/auth/me?orders")
                    .then(resp => {
                        const userOrder = resp.data.orders;
                        commit('user_order_receive', userOrder);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        deleteUserDish({commit}, id) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.delete("https://dev.api.eatoo.tech/dishes/deleteDish/" + id)
                    .then(resp => {
                        commit('user_dish_delete');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        updateUserDish({commit}, dishe) {
            axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.patch("https://dev.api.eatoo.tech/dishes/updateDish/" + dishe.id, dishe.data, config)
                    .then(resp => {
                        commit('user_dish_update');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        createOrder({commit}, order) {
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.post("https://dev.api.eatoo.tech/orders/create", order, config)
                    .then(resp => {
                        commit('create_order');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        modify({commit}, user) {
            axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.patch("https://dev.api.eatoo.tech/auth/me", user, config)
                    .then(resp => {
                        const user = resp.data.user;
                        commit('set_user', user);
                        commit('auth_modify', user);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        getMe({commit}, token) {
            localStorage.setItem('token', token);
            axios.defaults.headers.common['Authorization'] = "Bearer " + token;
            return new Promise((resolve, reject) => {
                commit('auth_request');
                axios.get("https://dev.api.eatoo.tech/auth/me", config)
                    .then(resp => {
                        commit('set_user', resp.data);
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        reject(err)
                    })
            })
        },
        logout({commit}) {
            return new Promise((resolve) => {
                commit('logout');
                localStorage.removeItem('token');
                delete axios.defaults.headers.common['Authorization'];
                resolve()
            })
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        userLoggedIn: state => state.user,
        authStatus: state => state.status,
    }
})
