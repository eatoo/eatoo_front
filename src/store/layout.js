export const layout = {
  namespaced: true,
  state: () => ({
    transparentNavbar: true
  }),
  actions: {},
  mutations: {
    setTransparentNavbar (state, transparent) {
      state.transparentNavbar = transparent
    }
  },
  getters: {},
}
