import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Notifications from 'vue-notification'
import { i18n } from './i18n'

import Vuetify from 'vuetify/lib';
import 'vuetify/dist/vuetify.min.css'
import vuetify from './plugins/vuetify'
import VuetifyAlgoliaPlaces from 'vuetify-algolia-places';
import { VueMaskDirective } from 'v-mask'

Vue.use(VuetifyAlgoliaPlaces, {
  algolia: {
  },
});

Vue.use(Notifications, {
  position: 'left'
});
Vue.use(VueAxios, axios);
Vue.directive('mask', VueMaskDirective);

export default new Vuetify({
  icons: {
    iconfont: 'md',
  },
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount('#app')
