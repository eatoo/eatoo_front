import Vue from 'vue'
import VueI18n from 'vue-i18n'
import en from './lang/en'
import fr from './lang/fr'

Vue.use(VueI18n);

const messages = {
    'en-GB': en,
    'fr-FR': fr,
};


export const i18n = new VueI18n({
    locale: Object.keys(messages).find(key => key === navigator.language) ? navigator.language : 'fr-FR',
    fallbackLocale: 'fr-FR', //set fallback local
    messages,
});
