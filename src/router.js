import Vue from 'vue'
import Store from './store'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login'
import Register from './views/Register'
import Secure from './views/Secure'
import Team from './views/Team'
import Contact from './views/Contact'
import About from './views/About'
import Privacy from './views/Privacy'
import Conditions from './views/Conditions'

import Profil from './views/Profil'
import CreateDish from './views/CreateDish'
import ModifyDish from './views/ModifyDish'
import Orders from './views/Orders'
import OrderConfirmation from "./views/OrderConfirmation";
import Payement from "./views/Payement";



Vue.use(Router);

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/secure',
      name: 'secure',
      component: Secure,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/team',
      name: 'team',
      component: Team,
      meta: {
        transparentNavbar: false
      }
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact,
      meta: {
        transparentNavbar: false
      }
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      meta: {
        transparentNavbar: false
      }
    },
    {
      path: '/privacy',
      name: 'privacy',
      component: Privacy,
      meta: {
        transparentNavbar: false
      }
    },
    {
      path: '/conditions',
      name: 'conditions',
      component: Conditions,
      meta: {
        transparentNavbar: false
      }
    },
    {
      path: '/profil',
      name: 'profil',
      component: Profil,
      meta: {
        requiresAuth: true,
        transparentNavbar: false
      }
    },
    {
      path: '/createdish',
      name: 'createdish',
      component: CreateDish,
      meta: {
        requiresAuth: true,
        transparentNavbar: false
      }
    },
    {
      path: '/modifydish',
      name: 'mydishes',
      component: ModifyDish,
      meta: {
        requiresAuth: true,
        transparentNavbar: false
      }
    },
    {
      path: '/myorders',
      name: 'myorders',
      component: Orders,
      meta: {
        requiresAuth: true,
        transparentNavbar: false
      }
    },
    {
      path: '/orderconfirmation',
      name: 'orderconfimation',
      component: OrderConfirmation,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/payment',
      name: 'payment',
      component: Payement,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      component: Home
    }
  ]
})

router.afterEach((to) => {
  const transparent = to.meta.transparentNavbar !== undefined ? to.meta.transparentNavbar : true
  Store.commit('layout/setTransparentNavbar', transparent)
})

router.beforeEach((to, from, next) => {
  window.scrollTo(0, 0)
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (Store.getters.isLoggedIn) {
      next()
      return
    }
    next({ name: 'login' })
  } else {
    next()
  }
})

export default router
