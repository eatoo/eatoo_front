# base image
FROM node:lts-alpine

# install simple http server for serving static content
RUN npm install -g http-server
RUN npm install -g @vue/cli@3.7.0

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY package.json /app/package.json

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN npm run build

EXPOSE 8080
CMD [ "http-server", "dist", "--proxy", "http://localhost:8080?" ]