module.exports = {
    moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
    transformIgnorePatterns: ["/node_modules/"],
    transform: {
        ".*\\.(vue)$": "vue-jest",
        "^.+\\.js$": "<rootDir>/node_modules/babel-jest"
    }
};